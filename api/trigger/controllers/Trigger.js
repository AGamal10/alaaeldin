'use strict';

var querystring = require('querystring');
var request = require('async-request');
/**
 * Trigger.js controller
 *
 * @description: A set of functions called "actions" for managing `Trigger`.
 */

module.exports = {

  /**
   * Retrieve trigger records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.trigger.search(ctx.query);
    } else {
      return strapi.services.trigger.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a trigger record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.trigger.fetch(ctx.params);
  },

  /**
   * Count trigger records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.trigger.count(ctx.query);
  },

  /**
   * Create a/an trigger record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.trigger.add(ctx.request.body);
  },

  /**
   * Update a/an trigger record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.trigger.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an trigger record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.trigger.remove(ctx.params);
  },

  callPhone: async (ctx) => {

    let dev = await strapi.controllers.device.find({
      query: {
        dev_id: ctx.request.body.dev_id
      }
    });

    if (dev.length === 0) {
      return {
        message: 'Device not registered'
      };
    } else {
      var form = {
        "To": dev[0].phone_number,
        "From": "+12018624557"
      };

      var formData = querystring.stringify(form);
      var contentLength = formData.length;

      let resp = await request('https://AC56f2405c78c50fc35377f6aa960adf05:8d0e7302275552b7db5608633e767b79@studio.twilio.com/v1/Flows/FWf8ccfd34dd80c3bf6aaecc164f86cca1/Executions', {
        headers: {
          'Content-Length': contentLength,
          'Content-Type': 'application/x-www-form-urlencoded'
        },
        data: formData,
        method: 'POST'
      });

      if (resp.statusCode !== 200) {
        let body = JSON.parse(resp.body);
        return {
          message: body.message
        };
      } else {
        strapi.controllers.trigger.create({
          request: {
            body: {
              dev_id: ctx.request.body.dev_id
            }
          }
        });
        return {
          message: 'Phone call sent'
        }
      }
    }
  }
};
