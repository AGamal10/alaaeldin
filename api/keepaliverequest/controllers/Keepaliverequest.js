'use strict';

/**
 * Keepaliverequest.js controller
 *
 * @description: A set of functions called "actions" for managing `Keepaliverequest`.
 */

module.exports = {

  /**
   * Retrieve keepaliverequest records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.keepaliverequest.search(ctx.query);
    } else {
      return strapi.services.keepaliverequest.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a keepaliverequest record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.keepaliverequest.fetch(ctx.params);
  },

  /**
   * Count keepaliverequest records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.keepaliverequest.count(ctx.query);
  },

  /**
   * Create a/an keepaliverequest record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.keepaliverequest.add(ctx.request.body);
  },

  /**
   * Update a/an keepaliverequest record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.keepaliverequest.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an keepaliverequest record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.keepaliverequest.remove(ctx.params);
  },

  keepAliveReq: async (ctx) => {
    let dev = ctx.query.dev_id;
    await strapi.controllers.keepaliverequest.create({
      request: {
        body: {
          dev_id: dev
        }
      }
    });

    return {
      firmware_version: '1.0.0'
    };
  }
};
